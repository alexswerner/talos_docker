ARG BASE_IMAGE=ubuntu
ARG BASE_TAG=bionic
ARG BASE=${BASE_IMAGE}:${BASE_TAG}
FROM ${BASE}
ARG UBUNTU_MIRROR=http:\\/\\/us.archive.ubuntu.com\\/ubuntu
#http:\/\/mirror.csclub.uwaterloo.ca

# do not query user during built, should be unset later
ENV DEBIAN_FRONTEND=noninteractive

# install basic set of utilities required for docker_entrypoint.sh
RUN DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true apt-get update && apt-get install -q -y --no-install-recommends \
        sudo \
        iproute2 \
        apt-transport-https tzdata \
    && rm -rf /var/lib/apt/lists/*

# set timezone
RUN echo "America/Toronto" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

# use local mirror for ubuntu packages
# if ubuntu mainline gets too unstable this can be switched
RUN echo "s/http:\/\/.*\.ubuntu\.com/${UBUNTU_MIRROR}/"
RUN sed -E -i "s/http:\/\/.*\.ubuntu\.com/${UBUNTU_MIRROR}/" /etc/apt/sources.list

# set default password (make sure ssh has root login disable)
RUN echo "root:root" | chpasswd

# install a set of utilities
RUN ln -fs /usr/share/zoneinfo/America/Toronto /etc/localtime
RUN DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true apt-get update && \
    apt-get install -q -y --no-install-recommends iputils-ping dnsutils vim-gtk sudo x11-xkb-utils xfce4-session build-essential usbutils less openssh-client nmap strace bash-completion zsh unzip wget sshfs cmake-curses-gui lsof ltrace lsof && \
    rm -rf /var/lib/apt/lists/*

# install more tools
RUN DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true apt-get update && \
    apt-get install -q -y --no-install-recommends python-pip python-scipy python-matplotlib && \
    rm -rf /var/lib/apt/lists/*

# setup entrypoint
COPY ./docker_entrypoint.sh /root

ENTRYPOINT ["/root/docker_entrypoint.sh"]
