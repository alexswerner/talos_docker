

Build nvidia layer for your Docker image

make BASE_IMAGE=XXXX nvidia

where XXXX is your docker base image like registry.gitlab.com/alexswerner/talos_docker/image:talos_kinetic_free 

This will create a docker image XXXX_nvidia which you can run with ./start.sh talos_kinetic_free_nvidia
